///////////////////////////////////////Service Account Credentials & Scopes//////////////////////////////////////////
var private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDZkH0VdYAz/p7c\n22Zfn7bbYScMM0hGcqyHs+90jcb8mVLYoewXifByksi0tTWls6qN6d/U/rpIKcJI\n1NoVPglEvE7SnBCeVPmmmmnl4t1uGShIErpHwmDRagCZ7raarCJpiiJNaA6uuNnD\nd2uM6hdwhDyGEvdTRjNeyVp0R2XgYY5CvnTiIpZK52sUY6ffiVuoIFSBnW5IgDVh\nO2k9stFibNbiEkMfkZXjC+5gU4byoKt4zeK2AH5i/vFqF+Lk+PWRjE0LdWLOzbe5\ny8RcU6h/EJeRfwRHnfL2MLSoEv6GAyVa9MIodN4X1j7JxTj/qjGzbGrjm8aD2Fyd\n8q1nMu1ZAgMBAAECggEAPiROBfYU00UT3SNxnziAQzMBszNmnnCcpPoZGCJyxq2I\ng0XkqGiG3ELevvbPRvAFdBlsDGthi8EZtDHPxKd+gDqFDRT0jjTZRePvAXGGK69V\n2Rg4SszTWJ09ZLpRP+aQwbJpz9A7+V3lHTab6pjmbwXmP6llKUzw2U8L3hCSob7U\nXx95hspxV8TIB8NYNTgU4+wy/oOlQsxbXZcshaWKdOIxXHtUXzWze0djySRPOnud\nFCC6jx+GYq6vw2j4Ogjfb5ailOvIrXO915OmExzcR9f95sVFs/uT7fOIyhn6SMVM\nYF96raivQVYSvIwcjoMMzXn6M2Q2hXSgCUyIgTWRnQKBgQD/+mJsrpA0kHGd6fif\nwOizrjs7GiXGgBGNWmPORW+aA1q4FI3qIEh9jVotCIY/S0CS7jdIGSbZoux5VkfD\nkLUQGGg4po6dvbxuKMggGW0VD381xFbhxpkwuBi+j76kx1WVPx+atPvsHvIq9HeC\nXg7yN2VJGl8rY8lTMSQtwssQ0wKBgQDZlULthBwfwLHXcUueVBi3PQrmkxk9YMYZ\nHSPqY3Co+L0Nd1Xdd/0InxFLZPTy4SUMlBUAVh32LIx6z67eEUdyGLz88itAMB6v\njqs+v4DLTlvmHpE6WfFMkUcsmUF8dTPtVBVxqyRqVXx7MAEnb4RPIuCce0q4gXTx\ndz9M6/GNowKBgAMP672BGRiH4dQRyHegxyffHbZHScxmG8+lFSySiBM/lwY8uLXk\nTPmzhzCiuXhoXg1j7CVgp6ZS3KFW9uTQBlp68EQ9OdeJPYfT1RENPNDTrqHmB2QM\nDCkZCrK85XrzJ4Lxjl87awgYFq2M3yEgHu2m2/9T4W+TcgUcwWIKwoMhAoGADPbw\n2vgxOM8hiaXNjKyUIG75tE11hou6ogpzdmbgcqEHmsLOkfixUg8wV563XbV+oKY+\nLhvzgxSadkLjt4WuDSVeNXA0lYmIeot0trNweCy+GxMdMi3jy90oMYccOM8+/dgx\nwukgrVlUpn5wwynI53I7kfdit2W3Ux/43Jt+dCMCgYAf6OH+otVdilXXnPKKn+9Q\npdGTIoZSV/XGFed5LtWXh9Y0AvxrTq8+DXzD9spnhZiESEcVA0EDy6Ko91MplO7a\nlhhBQn4K1TXuDcbATC5Rqfz/IxmPTR8b+S2BPdoeZASvZiT0QzObNTsLE17YgtnZ\npxvYHmLfEkpD1FG1KrQV2Q==\n-----END PRIVATE KEY-----\n"
var client_email = "connectors@smart-view-generic-api-beta.iam.gserviceaccount.com"; // client_email of JSON file retrieved by creating Service Account
var scopes = ['https://www.googleapis.com/auth/bigquery', 
              'https://www.googleapis.com/auth/userinfo.email', 
              'https://www.googleapis.com/auth/script.external_request', 
              'https://www.googleapis.com/auth/script.scriptapp', 
              'https://www.googleapis.com/auth/script.webapp.deploy', 
              'https://www.googleapis.com/auth/drive', 
              'https://www.googleapis.com/auth/script.cpanel',
              'https://www.googleapis.com/auth/cloud-platform',
              'https://spreadsheets.google.com/feeds',
              'https://www.googleapis.com/auth/spreadsheets'
             ];
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////-Get Data Function-////////////////////////////////////////////////////
/*
Generating service account access token for rest api authorization 
*/
function getAccessToken(){
  var url = "https://www.googleapis.com/oauth2/v3/token";
  var header = {
    alg: "RS256",
    typ: "service_account",
  };
  var now = Math.floor(Date.now() / 1000);
  var claim = {
    iss: client_email,
    scope: scopes.join(" "),
    aud: url,
    exp: (now + 3600).toString(),
    iat: now.toString(),
  };
  
  var signature = Utilities.base64Encode(JSON.stringify(header)) + "." + Utilities.base64Encode(JSON.stringify(claim));
  var jwt = signature + "." + Utilities.base64Encode(Utilities.computeRsaSha256Signature(signature, private_key));
  var params = {
    method: "post",
    payload: {
      assertion: jwt,
      grant_type: "urn:ietf:params:oauth:grant-type:jwt-bearer",
    },
  };
  var res = UrlFetchApp.fetch(url, params).getContentText();
  return JSON.parse(res).access_token;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////-Do Get Function-///////////////////////////////////////////////////////
/*
Get method function called from IoT platform containing parameters 
link=>the url link to the google spreadsheet to be modified
reportID=>the ID of the report/division to extract data for
run=>what should the script do, first time loading data to the spreadsheet - initial or an update to a existing spreadsheet with data - update
*/
function doGet(e) {
  var results = null;
  //checking the script run condition
  if(e.parameters.run=="initial") {
    results = getData(e);
  }else if(e.parameters.run=="update") {
    results = getUpdate(e);
  }
  
  return ContentService.createTextOutput(results).setMimeType(ContentService.MimeType.JAVASCRIPT);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////-Get Data Function-////////////////////////////////////////////////////
/*
Getting the initial select statement for a bigquery job from an external source
Printing the data to sheets & returning a response message
*/
function getData(e) {
  var result = null;
  var status = null;
  var code = 400;
  var message = null;
  var spreadsheet = SpreadsheetApp.openByUrl(e.parameters.link);
  var pulseSheet = spreadsheet.getSheetByName("Pulse Data");
  var thermoSheet = spreadsheet.getSheetByName("Thermo Data");
  
  try {
    //retrieve report select statement
    var options = {
      'method' : 'post',
      'contentType' : 'application/x-www-form-urlencoded'
    };
    var response = UrlFetchApp.fetch('https://connector-dot-smart-view-iot-management.appspot.com/connector/getStatement.php?thisID='+e.parameters.reportID, options);
    var statement = JSON.parse(response.getContentText());
    
    //requesting data from bigquery and printing to sheets
    insertInitialData(getAccessToken(), statement.utilities, pulseSheet);
    insertInitialData(getAccessToken(), statement.thermo, thermoSheet);
    //successful retrieval & printing
    code = 200;
    status = "success";
  } catch(err) {
    code = 404;
    status = "error";
    message = err.message;
  }
  
  result = JSON.stringify({
    "status": status,
    "code": code,
    "message": message
  });
  return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////-Get Update Function-//////////////////////////////////////////////////
/*
Getting the update select statement for a bigquery job from an external source
Appending the data to sheets & returning a response message
*/
function getUpdate(e) {
  var result = null;
  var status = null;
  var code = 400;
  var message = null;
  var spreadsheet = SpreadsheetApp.openByUrl(e.parameters.link);
  var pulseSheet = spreadsheet.getSheetByName("Pulse Data");
  var thermoSheet = spreadsheet.getSheetByName("Thermo Data");
  
  //getting the first row dateTime from the Pulse Data & Thermo Data sheets for update
  var pulseLastUpdate = new Date(pulseSheet.getSheetValues(2, 11, 1, 1));
  var thermoLastUpdate = new Date(thermoSheet.getSheetValues(2, 21, 1, 1));
    
  try {
    //fetch data after last update date
    var options = {
      'method' : 'post',
      'contentType' : 'application/x-www-form-urlencoded'
    };
    var response = UrlFetchApp.fetch('https://connector-dot-smart-view-iot-management.appspot.com/connector/getUpdateStatement.php?reportID='+e.parameters.reportID+'&lastThermoUpdate='+thermoLastUpdate.toLocaleString()+'&lastPulseUpdate='+pulseLastUpdate.toLocaleString(), options);
    var statement = JSON.parse(response.getContentText());
    
    //requesting updated data from bigquery and appending to sheets
    updateData(getAccessToken(), statement.utilities, pulseSheet);
    updateData(getAccessToken(), statement.thermo, thermoSheet);
    
    //successful retrieval & appending
    code = 200;
    status = "success";
  } catch(err) {
    code = 404;
    status = "error";
    message = err.message;
  }
  result = JSON.stringify({
    "status": status,
    "code": code,
    "message": message
  });
  return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////-Insert Initial Data Function-//////////////////////////////////////////////
/*
Requesting data from bigquery rest api using service account access token 
& printing the data to google sheets
*/
function insertInitialData(authToken, query, sheet){
  var headers = { "Authorization" : "Bearer " + authToken };
  var raw = JSON.stringify({"query":query,"useQueryCache":true});
  
  var requestOptions = {
    method: 'POST',
    headers: headers,
    contentType: 'application/json',
    payload: raw,
    followRedirects: true
  };
  var queryResults = UrlFetchApp.fetch("https://bigquery.googleapis.com/bigquery/v2/projects/smart-view-generic-api-beta/queries", requestOptions)
  queryResults = JSON.parse(queryResults.getContentText());
  var rows = queryResults.rows;
  
  //Append the to data array
  var data = new Array(rows.length);
  console.log("Loops started for Data");
  for (var i = 0; i < rows.length; i++) {
    var cols = rows[i].f;
    if(cols != null){
      data[i] = new Array(cols.length);
      for (var j = 0; j < cols.length; j++) {
        data[i][j] = cols[j].v;
      }
    }
  }
  
  if (rows) {
    // Append the headers.
    var headers = queryResults.schema.fields.map(function(field) {
      return field.name;
    });
    
    sheet.appendRow(headers);
    sheet.getRange(2, 1, rows.length, headers.length).setValues(data);
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////-Update Data Function-///////////////////////////////////////////////
/*
Requesting updated data from bigquery rest api using service account access token 
& appending the data to google sheets
*/
function updateData(authToken, query, sheet) {
  var headers = { "Authorization" : "Bearer " + authToken };
  var raw = JSON.stringify({"query":query,"useQueryCache":true});
  
  var requestOptions = {
    method: 'POST',
    headers: headers,
    contentType: 'application/json',
    payload: raw,
    followRedirects: true
  };
  var queryResults = UrlFetchApp.fetch("https://bigquery.googleapis.com/bigquery/v2/projects/smart-view-generic-api-beta/queries", requestOptions)
  queryResults = JSON.parse(queryResults.getContentText());
  var rows = queryResults.rows;
  
  //Append the to data array
  var data = new Array(rows.length);
  for (var i = 0; i < rows.length; i++) {
    var cols = rows[i].f;
    if(cols != null){
      data[i] = new Array(cols.length);
      for (var j = 0; j < cols.length; j++) {
        data[i][j] = cols[j].v;
      }
    }
  }
  
  if(rows){
    var headers = queryResults.schema.fields.map(function(field) {
      return field.name;
    });
    
    sheet.getRange(2, 1, rows.length, headers.length).setValues(data);
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////